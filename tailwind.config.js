/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './app.vue'
  ],
  theme: {
    extend: {
      colors: {
        "primary": "#f60",
        "custom-blue": "#4f8ad0",
        "custom-dark-grey": "#4d4d4d",
        "custom-red": "#d60000",
        "custom-grey": "#828282",
        "custom-light-grey": "#ededed",
        "custom-yellow": "#fcc12d",
        "black-0.2": "rgba(0, 0, 0, .2)"
      },
      gridTemplateColumns: {
        'auto-fit': "repeat(auto-fit,minmax(200px,1fr))"
      },
    }

    // fontFamily: {
    //   sans: ['Montserrat'],
    //   serif: ['Montserrat'],
    //   mono: ['Montserrat'],
    //   display: ['Montserrat'],
    //   body: ['Montserrat']
    // }
  },
  plugins: []
}
