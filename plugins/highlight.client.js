import hljs from 'highlight.js/lib/core'
import javascript from 'highlight.js/lib/languages/javascript'
import xml from 'highlight.js/lib/languages/xml'
import highlightJS from '@highlightjs/vue-plugin'
import 'highlight.js/styles/base16/atelier-cave-light.css'

export default defineNuxtPlugin((nuxtApp) => {
    hljs.registerLanguage('javascript', javascript)
    hljs.registerLanguage('xml', xml)

    nuxtApp.vueApp.use(highlightJS)
})
