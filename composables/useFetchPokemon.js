export default async (filters) => {
    const { data, error, pending } = await usePokeapiData(`/pokemon`, {
            ...filters
        }
    )
    
    if (error.value) {
        throw createError({
            ...error.value,
            statusMessage: "Unable to fetch pokemon"
        })
    }

    return {data, error, pending}
}
